FROM openjdk:8
WORKDIR /app
ADD target/simpleobjectstorage.jar simpleobjectstorage.jar
ADD src/main/resources/application.properties application.properties
EXPOSE 8080
#ARG JAR_FILE
#COPY ${JAR_FILE} simpleobjectstorage.jar
RUN mkdir data-local
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "simpleobjectstorage.jar"]
