package simpleobjectstorage.domain;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.Set;

@Document
public class Bucket {

    @Id
    private ObjectId id;
    private String bucketName;
    private Set<ObjFile> objFiles;
    private String bucketUUID;
    private Long modified;
    private Long created;

    public Bucket(String bucketName, String bucketUUID, Long created, Long modified) {
        this.bucketName = bucketName;
        this.bucketUUID = bucketUUID;
        this.created = created;
        this.modified = modified;
    }


    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
        this.modified = Instant.now().toEpochMilli();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
        this.modified = Instant.now().toEpochMilli();
    }

    public Set<ObjFile> getObjFiles() {
        return objFiles;
    }

    public void setObjFiles(Set<ObjFile> objFiles) {
        this.objFiles = objFiles;
        this.modified = Instant.now().toEpochMilli();
    }

    public String getBucketUUID() {
        return bucketUUID;
    }

    public void setBucketUUID(String bucketUUID) {
        this.bucketUUID = bucketUUID;
        this.modified = Instant.now().toEpochMilli();
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }
}