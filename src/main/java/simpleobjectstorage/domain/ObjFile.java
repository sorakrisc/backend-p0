package simpleobjectstorage.domain;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

@Document
public class ObjFile {
    @Id
    private ObjectId id;
    private String objFileName;
    private String ticket;
    private String fileType;
    private Set<String> ObjFileParts;
    private String objUUID;
    private Long modified;
    private Long created;
    private Long totalContentLength;
    private HashMap<Integer, ArrayList<Object>> objFileMD5Part;
    private HashMap<String, String> metaData;

    public ObjFile(){
    }

    public ObjFile(String objFileName, String ticket, String objUUID, Long modified, Long created, Long totalContentLength, HashMap<String, String> metaData) {
        this.objFileName = objFileName;
        this.ticket = ticket;
        this.objUUID =objUUID;
        this.modified = modified;
        this.created = created;
        this.totalContentLength = totalContentLength;
        this.metaData = metaData;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
        this.modified = Instant.now().toEpochMilli();
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
        this.modified = Instant.now().toEpochMilli();
    }

    public String getObjFileName() {
        return objFileName;
    }

    public void setObjFileName(String objFileName) {
        this.objFileName = objFileName;
        this.modified = Instant.now().toEpochMilli();
    }

    public Set<String> getObjFileParts() {
        return ObjFileParts;
    }

    public void setObjFileParts(Set<String> objFileParts) {
        ObjFileParts = objFileParts;
        this.modified = Instant.now().toEpochMilli();
    }

    public Long getModified() {
        return modified;
    }

    public void setModified(Long modified) {
        this.modified = modified;
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getObjUUID() {
        return objUUID;
    }

    public void setObjUUID(String objUUID) {
        this.objUUID = objUUID;
        this.modified = Instant.now().toEpochMilli();
    }

    public HashMap<Integer, ArrayList<Object>> getObjFileMD5Part() {
        return objFileMD5Part;
    }

    public void setObjFileMD5Part(HashMap<Integer, ArrayList<Object>> objFileMD5Part) {
        this.objFileMD5Part = objFileMD5Part;
        this.modified = Instant.now().toEpochMilli();
    }

    public Long getTotalContentLength() {
        return totalContentLength;
    }

    public void setTotalContentLength(Long totalContentLength) {
        this.totalContentLength = totalContentLength;
        this.modified = Instant.now().toEpochMilli();
    }

    public HashMap<String, String> getMetaData() {
        return metaData;
    }

    public void setMetaData(HashMap<String, String> metaData) {
        this.metaData = metaData;
        this.modified = Instant.now().toEpochMilli();
    }
}
