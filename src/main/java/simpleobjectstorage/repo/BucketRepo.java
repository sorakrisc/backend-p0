package simpleobjectstorage.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import simpleobjectstorage.domain.Bucket;
public interface BucketRepo extends MongoRepository<Bucket, Integer> {
    public Bucket findByBucketName(String bucketName);
}