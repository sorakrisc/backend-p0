package simpleobjectstorage.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import simpleobjectstorage.domain.ObjFile;

public interface ObjFileRepo extends MongoRepository<ObjFile, Integer> {
    public ObjFile findByObjFileName(String objFileName);
}