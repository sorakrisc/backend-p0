package simpleobjectstorage.service;


import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simpleobjectstorage.domain.Bucket;
import simpleobjectstorage.domain.ObjFile;
import simpleobjectstorage.repo.BucketRepo;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

@Service("bucketService")
public class BucketService {
    @Autowired
    private BucketRepo bucketRepo;

    public String dataStorage = "/app/data-local/";

    // check if bucketname is valid
    // {bucketName} should be alpha-numeric and may contain underscore (_) or hyphen (-)
    public boolean isBucketNameValid(String bucketName){
        String pattern = "^[-_a-zA-Z0-9]*$";
        return bucketName.matches(pattern);
    }

    // check if bucketname already exist
    public boolean isBucketNameExist(String bucketName){
        return bucketRepo.findByBucketName(bucketName) != null;
    }

    public Bucket createNewBucket(String bucketName, String randomUUID){
        Long now = Instant.now().toEpochMilli();
        return new Bucket(bucketName, randomUUID , now, now);
    }
    public boolean deleteBucketFromLocal(String bucketName){
        try {
            FileUtils.deleteDirectory(new File(dataStorage + bucketName));
           return true;
        } catch (Exception e){
            return false;
        }
    }
    public Boolean saveBucketToLocal(String bucketName){
        try {
            // returns pathnames for files and directory
            File f = new File(dataStorage+bucketName);
            // create
            return f.mkdir();

        } catch(Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public HashMap<String, Object> getBucketMetaData(Bucket bucket){
        HashMap<String, Object> map = new HashMap<>();
        map.put("created", bucket.getCreated());
        map.put("modified", bucket.getModified());
        map.put("name", bucket.getBucketName());
        return map;
    }
    // get bucket and validate it's name
    public Bucket getBucket(String bucketName){
        if(isBucketNameValid(bucketName)) {
            return bucketRepo.findByBucketName(bucketName);
        }
        return null;
    }

    public ArrayList<HashMap<String, Object>> getBucketObjectList(Bucket bucket) {
        ArrayList<HashMap<String, Object>> objects= new ArrayList<>();
        for (ObjFile objfile: bucket.getObjFiles()) {
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", objfile.getObjFileName());
            map.put("created", objfile.getCreated());
            map.put("modified", objfile.getModified());
            objects.add(map);
        }
        return objects;
    }
}
