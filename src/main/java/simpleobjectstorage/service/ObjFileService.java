package simpleobjectstorage.service;

import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import simpleobjectstorage.domain.Bucket;
import simpleobjectstorage.domain.ObjFile;
import simpleobjectstorage.repo.BucketRepo;
import simpleobjectstorage.repo.ObjFileRepo;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service("objectService")
public class ObjFileService {

//    private ObjFileRepo objFileRepo;
//
//    public ObjFileService(ObjFileRepo objFileRepo){
//        this.objFileRepo = objFileRepo;
//    }
    @Autowired
    private ObjFileRepo objFileRepo;

    @Autowired
    private BucketRepo bucketRepo;
    public String dataStorage = "data-local/";

    public static ArrayList<Long> getRange(String range) {
        String patternString = "^([0-9]+)[-]([0-9]*)$";
        ArrayList<Long> lst = new ArrayList<>();
        if(range.matches(patternString)){
            String arr[] =range.split("-");
            lst.add(Long.parseLong(arr[0]));
            if(arr.length == 2){
                lst.add(Long.parseLong(arr[1]));
            }
            return lst;
        }
        return null;
    }

    public boolean isValidMD5(String clientMD5, String path){
        try {
            File file = new File(path);
            InputStream is = new FileInputStream(file);
            String md5 = DigestUtils.md5DigestAsHex(is);
            if (md5.equals(clientMD5)){
                return true;
            }
            return false;
        }
        catch (IOException err){
            return false;
        }
    }
    public Boolean storeFile(HttpServletRequest request, String bucketUUID, String objFileUUID, Integer part, String clientMD5){
        try {
            String path = dataStorage+bucketUUID+"/"+objFileUUID+"/"+part.toString();
            FileOutputStream fOut = new FileOutputStream(path);
            ServletInputStream inputStream = request.getInputStream();
            MessageDigest md = MessageDigest.getInstance("MD5");
            DigestInputStream dis = new DigestInputStream(inputStream, md);
            IOUtils.copy(dis, fOut);

            byte[] digest = md.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                String hex = Integer.toHexString(0xff & digest[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            fOut.close();
            inputStream.close();
            dis.close();
            if (clientMD5.equals(hexString.toString())){
                return true;
            }
            return null;
        } catch(Exception err){
            return false;
        }
    }

    // check if bucketname is valid
    // {objFileName} should be alpha-numeric and may contains dot (.) or underscore (_) or or hyphen (-),
    // but cannot begin or end with dot.
    public boolean isObjFileNameValid(String objFileName){
        String pattern = "^[-_a-zA-Z0-9][-_a-zA-Z0-9\\.]*[-_a-zA-Z0-9]$";
        return objFileName.matches(pattern);
    }
    public ObjFile getFile(Bucket bucket, String objectName) {
        if(isObjFileNameValid(objectName)) {
            return getMatchObjFileFromSet(bucket.getObjFiles(), objectName);
        }
        return null;
    }

    public ObjFile getMatchObjFileFromSet(Set<ObjFile> setObjFiles, String objName){
        if(setObjFiles !=null) {
            for (ObjFile ech : setObjFiles) {
                if (ech.getObjFileName().equals(objName)) {
                    return ech;
                }
            }
        }
        return null;
    }

    private static String calculateChecksumForMultipartUpload(List<String> md5s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String md5:md5s) {
            stringBuilder.append(md5);
        }
        String hex = stringBuilder.toString();
        byte raw[] = BaseEncoding.base16().decode(hex.toUpperCase());
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putBytes(raw);
        String digest = hasher.hash().toString();

        return digest + "-" + md5s.size();
    }
    public String getCheckSum(ObjFile objFile){
        try {
            List<String> md5s = new ArrayList<>();
            HashMap<Integer, ArrayList<Object>> partMap = objFile.getObjFileMD5Part();
            for (Integer part : partMap.keySet()) {
                ArrayList<Object> objects = partMap.get(part);
                md5s.add(objects.get(0).toString());
            }
            String checkSum = calculateChecksumForMultipartUpload(md5s);
            return checkSum;
        } catch(Exception err){
            return null;
        }
    }
    public HashMap<String, Object> generateRetCompleteFile(String eTag, Long contentLength, String objFileName, String error){
        HashMap<String, Object> ret = new HashMap<>();
        ret.put("eTag",eTag);
        ret.put("length", contentLength);
        ret.put("name", objFileName);
        if(error != null){
            ret.put("error", error);
        }
        return ret;
    }

    public HashMap<String, Object> generateRetUploadFile(String contentMD5, Long contentLength, Integer partNumber, String error){
        HashMap<String, Object> ret = new HashMap<>();
        ret.put("md5", contentMD5);
        ret.put("length", contentLength);
        ret.put("partNumber", partNumber);
        if(error != null) {
            ret.put("error", error);
        }
        return ret;
    }
    public Boolean saveObjFileFolderToLocal(String bucketUUID, String objectUUID){
        try {
            // returns pathnames for files and directory
            File f = new File(dataStorage+bucketUUID+"/"+objectUUID);
            // create
            return f.mkdir();

        } catch(Exception e) {
            // if any error occurs
            return false;
        }
    }
    public ObjFile createNewObjFile(Bucket bucket, String objectName) {
        Set<ObjFile> setObjFiles = bucket.getObjFiles();
        ObjFile objFile = getMatchObjFileFromSet(setObjFiles, objectName);
        if(objFile == null) {
            String randomUUID = UUID.randomUUID().toString();
            if(saveObjFileFolderToLocal(bucket.getBucketUUID(), randomUUID)) {
                Long now = Instant.now().toEpochMilli();
                return new ObjFile(objectName, "incomplete", randomUUID, now, now, 0L, new HashMap<>());
            }
        }
        return null;
    }
    public Boolean updateTicketTo(Bucket bucket, ObjFile objFile,String ticketStatus){
        try {
            Set<ObjFile> set = bucket.getObjFiles();
            for (ObjFile echFile : set) {
                if(echFile.equals(objFile)) {
                    echFile.setTicket(ticketStatus);
                    bucket.setObjFiles(set);
                    bucketRepo.save(bucket);
                    return true;
                }
            }
        } catch (DataAccessException err){
            return false;
        }
        return false;
    }
    public Boolean addObjFilePart(Bucket bucket, String objectName, Integer part, String md5, Long contentLength, Long oldContentLength) {
        try {
            Set<ObjFile> set = bucket.getObjFiles();
            for (ObjFile echFile : set) {
                if (echFile.getObjFileName().equals(objectName)) {
                    HashMap<Integer, ArrayList<Object>> mapPartMD5 = echFile.getObjFileMD5Part();
                    if (mapPartMD5 == null) {
                        mapPartMD5 = new HashMap<>();
                    }
                    ArrayList<Object> lst = new ArrayList<>();
                    lst.add(md5);
                    lst.add(contentLength);
                    mapPartMD5.put(part, lst);
                    echFile.setObjFileMD5Part(mapPartMD5);
                    if(oldContentLength == null) {
                        echFile.setTotalContentLength(echFile.getTotalContentLength() + contentLength);
                    } else {
                        echFile.setTotalContentLength(echFile.getTotalContentLength() -oldContentLength + contentLength);
                    }
                    bucket.setObjFiles(set);
                    bucketRepo.save(bucket);
                    return true;
                }
            }
        } catch (DataAccessException err){
            return false;
        }
        return false;

    }
    public Boolean isValidPartNumber(ObjFile objFile, Integer part){
        if (part>=1 && part<=10000) {
            if (objFile.getObjFileMD5Part().get(part)!=null){
                return true;
            }
        }
        return false;
    }

    public boolean deletePartFromLocal(String bucketUUID, String objUUID, Integer partNumber) {
        String path = dataStorage+bucketUUID+"/"+objUUID+"/"+partNumber;
        File file = new File(path);
        return file.delete();
    }

    public String getFileByPart(String bUUID, String oUUID, String part){
        return dataStorage+bUUID+"/"+oUUID+"/"+part;
    }
    public ResponseEntity sendFileToClient(Bucket bucket, ObjFile objFile, Long init, Long end, HttpServletResponse HSR) {
        try {
            String bUUID = bucket.getBucketUUID();
            String oUUID = objFile.getObjUUID();

            HashMap<Integer, ArrayList<Object>> md5PartLst = objFile.getObjFileMD5Part();
            ArrayList<Integer> partNames = new ArrayList<>(md5PartLst.keySet());
//            Integer partCount = 1;
//            Integer partContentLength = Integer.parseInt(md5PartLst.get(partNames.get(partCount)).get(1).toString());

            OutputStream os = new BufferedOutputStream(HSR.getOutputStream());
//            String pathToFile = dataStorage + bUUID + "/" + oUUID + "/" + partNames.get(partCount);
//            InputStream is = new FileInputStream(pathToFile);
            List<InputStream> lstIS = new ArrayList<>();
            for (Integer ech : partNames) {
                lstIS.add(new FileInputStream(getFileByPart(bUUID, oUUID, ech.toString())));
            }
            SequenceInputStream s = new SequenceInputStream(Collections.enumeration(lstIS));
//            if(init > 0) {
//                s.skip(init);
//            }
//            byte[] buffer = new byte[512];
//            int bytesRead;
//            while ((bytesRead = s.read(buffer)) != -1) {
//                os.write(bytesRead);
//            }
//            os.write(bytesRead);
            if(init>0) {
                s.skip(init);
            }
            // write what read
            int count =0;
            for (Long i = init; i <= end; i++) {
                count++;
                os.write(s.read());
            }
            os.close();
            return ResponseEntity.ok().build();
//                .headers(headers)
//                    .contentLength(file.length())
//                    .contentType(MediaType.parseMediaType("application/octet-stream"))
//                    .body(os);
        } catch (Exception err){
            System.out.println("err: "+err);
            return ResponseEntity.badRequest().build();
        }
    }
    private byte[] concatenateByteArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    public Long getPartContentLength(ObjFile objFile, Integer partNumber) {
        try {
            return (Long) objFile.getObjFileMD5Part().get(partNumber).get(1);
        } catch (NullPointerException err){
            return null;
        }
    }
}
