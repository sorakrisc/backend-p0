package simpleobjectstorage.controller;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import simpleobjectstorage.domain.Bucket;
import simpleobjectstorage.repo.BucketRepo;
import simpleobjectstorage.service.BucketService;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
public class BucketController {
    @Autowired
    private BucketRepo bucketRepo;

    @Autowired
    private BucketService bucketService;


    @GetMapping("/all")
    public List<Bucket> getAll() {
        return bucketRepo.findAll();
    }
    @DeleteMapping(value = "/deleteall")
    public ResponseEntity deleteAll(){
        bucketRepo.deleteAll();
        return ResponseEntity.ok("all deleted");

    }

    //method post - create a bucket
    @PostMapping(value = "/{bucketname}", params = "create")
    public ResponseEntity createBucket(@PathVariable("bucketname") String bucketname) {
        bucketname=bucketname.toLowerCase();
        if(bucketService.isBucketNameValid(bucketname) && !bucketService.isBucketNameExist(bucketname)) {
            String randomUUID =UUID.randomUUID().toString();
            if( bucketService.saveBucketToLocal(randomUUID)) {
                Bucket bucket = bucketService.createNewBucket(bucketname, randomUUID);

                try {
                    bucketRepo.save(bucket);
                    return ResponseEntity.ok(bucketService.getBucketMetaData(bucket));
                } catch (DataAccessException err) {
                    return ResponseEntity.badRequest().body("cannot be created");
                }
            }
        }

        return ResponseEntity.badRequest().body("bucket already exists OR invalid bucket name");

    }

    //method delete - delete the entire bucket
    @DeleteMapping(value = "/{bucketname}", params = "delete")
    public ResponseEntity deleteBucket( @PathVariable("bucketname") String bucketname) {
        bucketname=bucketname.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketname);
        if (bucket != null) {
            try {
                String bucketUUID = bucket.getBucketUUID();
                bucketRepo.delete(bucket);
                bucketService.deleteBucketFromLocal(bucketUUID);
                return ResponseEntity.ok(bucketname + " is deleted.");
            } catch (DataAccessException err) {
                return ResponseEntity.badRequest().body("bucket does not exist OR cannot be deleted");
            }
        }

        return ResponseEntity.badRequest().body("bucket does not exist OR cannot be deleted");

    }

    //method get - list object of a bucket
    @GetMapping(value = "/{bucketname}", params = "list")
    public ResponseEntity listBucket( @PathVariable("bucketname") String bucketname) {
        bucketname=bucketname.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketname);
        if(bucket!=null) {
            try {
                HashMap<String, Object> retJSON = bucketService.getBucketMetaData(bucket);
                //TODO: add actual objects
                retJSON.put("objects", bucketService.getBucketObjectList(bucket));

                return ResponseEntity.ok(retJSON);
            } catch (DataAccessException err) {
                return ResponseEntity.badRequest().body("bucket does not exist OR cannot be listed");
            }
        }
        return ResponseEntity.badRequest().body("bucket does not exist OR cannot be listed");

    }


}