package simpleobjectstorage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.dao.DataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import simpleobjectstorage.domain.Bucket;
import simpleobjectstorage.domain.ObjFile;
import simpleobjectstorage.repo.BucketRepo;
import simpleobjectstorage.repo.ObjFileRepo;
import simpleobjectstorage.service.BucketService;
import simpleobjectstorage.service.ObjFileService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@RestController
public class ObjFileController {

//    private BucketRepo bucketRepo;
//    private BucketService bucketService;
//    public ObjectController(BucketRepo bucketRepo, BucketService bucketService){
//        this.bucketRepo = bucketRepo;
//        this.bucketService = bucketService;
//    }
    @Autowired
    private ObjFileService objFileService;
    @Autowired
    private BucketService bucketService;
    @Autowired
    private ObjFileRepo objFileRepo;
    @Autowired
    private BucketRepo bucketRepo;

    //method post - create a ObjFile
    @PostMapping(value = "/{bucketName}/{objectName}", params = "create")
    public ResponseEntity createObject( @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName){
        bucketName=bucketName.toLowerCase();
        objectName=objectName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if(bucket!=null) {
            try {
                ObjFile objFile = objFileService.createNewObjFile(bucket, objectName);
                if(objFile == null){
                    return ResponseEntity.badRequest().body("object already exist");
                }
                Set<ObjFile> objFiles = bucket.getObjFiles();
                if(objFiles == null){
                    objFiles = new HashSet<>();
                }
                objFiles.add(objFile);
                bucket.setObjFiles(objFiles);
                bucketRepo.save(bucket);
//                objFileRepo.save(objFile);
                return ResponseEntity.ok("hi");
            } catch (DataAccessException err){
                return ResponseEntity.status(400).build();
            }
        }
        else {
            return ResponseEntity.badRequest().body("bucket does not exist or object already exist");
        }
    }


    //method put - step 2 upload all part create a ObjFile
    @PutMapping(value = "/{bucketName}/{objectName}", params = "partNumber")
    public ResponseEntity uploadObjectByPart(
            HttpServletRequest request,
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName, @RequestParam("partNumber") Integer partNumber,
            @RequestHeader("Content-Length") Long contentLength, @RequestHeader("Content-MD5")  String contentMD5) {
        if(partNumber<1 || partNumber>10000 ){
            return ResponseEntity.badRequest().body(objFileService.generateRetUploadFile(contentMD5, contentLength,partNumber, "InvalidPartNumber"));
        }
        objectName = objectName.toLowerCase();
        bucketName = bucketName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if (bucket != null) {
            ObjFile objFile = objFileService.getFile(bucket, objectName);
            if (objFile !=null && objFile.getTicket().equals("incomplete")) {
                Long oldContentLength = objFileService.getPartContentLength(objFile, partNumber);
                Boolean err = objFileService.storeFile(request,bucket.getBucketUUID(), objFile.getObjUUID(), partNumber, contentMD5);
                if(err == null) {
                    return ResponseEntity.badRequest().body(objFileService.generateRetUploadFile(contentMD5,contentLength,partNumber, "MD5Mismatched"));
                }
                else if (err) {
                    if(objFileService.addObjFilePart(bucket, objectName, partNumber, contentMD5, contentLength, oldContentLength)) {
                        HashMap<String, Object> ret = new HashMap<>();
                        ret.put("md5", contentMD5);
                        ret.put("length", contentLength);
                        ret.put("partNumber", partNumber);
                        return ResponseEntity.ok(objFileService.generateRetUploadFile(contentMD5, contentLength, partNumber, null));
                    }
                }
                return ResponseEntity.status(400).build();
            }
            else {
                return ResponseEntity.badRequest().body(objFileService.generateRetUploadFile(contentMD5, contentLength,partNumber, "InvalidObject"));
            }
        }
        else {
            return ResponseEntity.badRequest().body(objFileService.generateRetUploadFile(contentMD5, contentLength,partNumber, "InvalidBucket"));
        }
    }


    @PostMapping(value = "/{bucketName}/{objectName}", params = "complete")
    public ResponseEntity completeUploadFile(
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName) {
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if(bucket !=null) {
            ObjFile objFile = objFileService.getFile(bucket, objectName);
            if (objFile != null && objFile.getTicket().equals("incomplete")) {

                Long contentLength =objFile.getTotalContentLength();
                String eTag = objFileService.getCheckSum(objFile);

                objFile.setTicket("complete");
                if(objFileService.updateTicketTo(bucket, objFile, "complete")) {
                    return ResponseEntity.ok(objFileService.generateRetCompleteFile(eTag, contentLength, objectName, null));
                }
            }
            else {
                return ResponseEntity.badRequest().body(objFileService.generateRetCompleteFile("", 0L, objectName, "InvalidObjectName"));
            }
        }
        else {
            return ResponseEntity.badRequest().body(objFileService.generateRetCompleteFile("", 0L, objectName, "InvalidBucket"));
        }

        return ResponseEntity.status(400).build();
    }

    @DeleteMapping(value = "/{bucketName}/{objectName}", params = "partNumber")
    public ResponseEntity deletePart(
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName,
            @RequestParam("partNumber") Integer partNumber) {
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if(bucket !=null) {
            ObjFile objFile = objFileService.getFile(bucket, objectName);
            if (objFile != null && objFile.getTicket().equals("incomplete")) {
                if(objFileService.isValidPartNumber(objFile, partNumber)){
                    if(objFileService.deletePartFromLocal(bucket.getBucketUUID(), objFile.getObjUUID(), partNumber)){
                        bucket.getObjFiles().remove(objFile);
                        objFile.getObjFileMD5Part().remove(partNumber);
                        bucket.getObjFiles().add(objFile);
                        bucketRepo.save(bucket);
                        return ResponseEntity.ok().build();
                    }

                }
            }

        }
        return ResponseEntity.badRequest().build();

    }

    @DeleteMapping(value = "/{bucketName}/{objectName}", params = "delete")
    public ResponseEntity deleteObject(
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName){
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if(bucket !=null) {
            ObjFile objFile = objFileService.getFile(bucket, objectName);
            if (objFile != null) {

                bucket.getObjFiles().remove(objFile);
                bucketRepo.save(bucket);
                return ResponseEntity.ok().build();

            }

        }
        return ResponseEntity.badRequest().build();
    }



    @GetMapping("/{bucketName}/{objectName}")
    public ResponseEntity downloadObject(
            HttpServletResponse response,
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName,
            @RequestHeader(value="Range", required = false) String range){
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        Bucket bucket = bucketService.getBucket(bucketName);
        if(bucket !=null) {
            ObjFile objFile = objFileService.getFile(bucket, objectName);
            if (objFile != null && objFile.getTicket().equals("complete")) {
                Long contentLength = objFile.getTotalContentLength();
//                System.out.println(range);
//                System.out.println(range.length());
                if(range!=null) {
                    range = range.substring(6);
                    ArrayList<Long> rangeLst = objFileService.getRange(range);
                    if(rangeLst!=null) {
                        if (rangeLst.size() == 2) {
                            Long A = rangeLst.get(0);
                            Long B = rangeLst.get(1);
                            if (A <= B && A<contentLength && B<contentLength) {
                                return objFileService.sendFileToClient(bucket, objFile, A, B, response);
                            }
                            System.out.println("bad range range must be A<=B");
                        }
                        else if(rangeLst.size() == 1) {
                            Long A = rangeLst.get(0);
                            if(A<contentLength) {
                                return objFileService.sendFileToClient(bucket, objFile, A, contentLength-1, response);
                            }
                        }
                        System.out.println("bad range size Rangesize= "+rangeLst.size());
                    }
                }
                else {
                    return objFileService.sendFileToClient(bucket, objFile, 0L, contentLength-1, response);
                }

            }
        }


        return ResponseEntity.badRequest().body("badddd");
    }

    @PutMapping(value="/{bucketName}/{objectName}", params = "metadata")
    public ResponseEntity putObjectMetadata(
        @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName,
        @RequestParam("key") String key,  @RequestBody String data){
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        if((key !=null && key.length()>0) && (data!=null && data.length()>0)) {
            Bucket bucket = bucketService.getBucket(bucketName);
            if (bucket != null) {
                ObjFile objFile = objFileService.getFile(bucket, objectName);
                if (objFile != null) {
                    bucket.getObjFiles().remove(objFile);
                    HashMap<String, String> map = objFile.getMetaData();
                    map.put(key, data);
                    objFile.setMetaData(map);
                    bucket.getObjFiles().add(objFile);
                    bucketRepo.save(bucket);
                    return ResponseEntity.ok().build();
                }
            }
        }

        return ResponseEntity.notFound().build();


    }

    @DeleteMapping(value="/{bucketName}/{objectName}", params = "metadata")
    public ResponseEntity deleteObjectMetadata(
            @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName,
            @RequestParam("key") String key){
        bucketName = bucketName.toLowerCase();
        objectName = objectName.toLowerCase();
        if (key !=null && key.length()>0) {
            Bucket bucket = bucketService.getBucket(bucketName);
            if (bucket != null) {
                ObjFile objFile = objFileService.getFile(bucket, objectName);
                if (objFile != null) {
                    bucket.getObjFiles().remove(objFile);
                    objFile.getMetaData().remove(key);
                    bucket.getObjFiles().add(objFile);
                    bucketRepo.save(bucket);
                    return ResponseEntity.ok().build();
                }

            }
        }


        return ResponseEntity.notFound().build();
    }

    @GetMapping(value="/{bucketName}/{objectName}", params = "metadata")
    public ResponseEntity getObjectMetadata( @PathVariable("bucketName") String bucketName, @PathVariable("objectName") String objectName,
                                             @RequestParam(value="key", required = false) String key) {
            bucketName = bucketName.toLowerCase();
            objectName = objectName.toLowerCase();
            Bucket bucket = bucketService.getBucket(bucketName);
            if (bucket != null) {
                ObjFile objFile = objFileService.getFile(bucket, objectName);
                if (objFile != null) {
                    if (key !=null && key.length()>0) {
                        HashMap<String, String> retJSON = new HashMap<>();
                        String val = objFile.getMetaData().get(key);
                        if (val != null) {
                            retJSON.put(key, val);
                        }
                        return ResponseEntity.ok(retJSON);
                    } else if(key == null){
                        return ResponseEntity.ok(objFile.getMetaData());
                    }
                }
            }

        return ResponseEntity.notFound().build();
    }


}