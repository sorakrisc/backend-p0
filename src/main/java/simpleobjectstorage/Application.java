package simpleobjectstorage;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class Application {

//    @Bean
//    public WebMvcConfigurer CORSConfigurer() {
//        return new WebMvcConfigurerAdapter() {
//            @Override
//            public void addCorsMappings(CorsRegistry registry) {
//                registry
//                        .addMapping("/**")
//                        .allowCredentials(true)
//                        .allowedMethods("*")
//                        .allowedHeaders("Authorization", "Content-Type")
//                        .allowedOrigins("https://fonytas.github.io");
//            }
//        };
//    }
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}